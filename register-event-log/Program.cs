﻿using Microsoft.Win32;
using System;

namespace register_event_log
{
	class Program
	{
		static void Main( string[] args )
		{
			string eventLogName = string.Empty;
			try
			{
				if ( args.Length != 1 )
				{
					throw new ArgumentException( "EventLog name must be passed as parameter." );
				}

				eventLogName = args[0];

				RegistryKey key = Registry.LocalMachine.OpenSubKey( 
					"SYSTEM\\CurrentControlSet\\Services\\Eventlog", true );
				key.CreateSubKey( eventLogName );
				Console.WriteLine( $"EventLog {eventLogName} created successfuly." );
			}
			catch ( Exception ex )
			{
				Console.WriteLine( $"Error while registering EventLog \"{eventLogName}\": {ex.Message}" );
			}
		}
	}
}
